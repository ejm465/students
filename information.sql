-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2021 at 03:58 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `information`
--

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `Student_Number` varchar(20) NOT NULL,
  `Firstname` varchar(50) NOT NULL,
  `Surname` varchar(50) NOT NULL,
  `Course_Code` varchar(20) NOT NULL,
  `Course_Description` varchar(100) NOT NULL,
  `Grade` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`Student_Number`, `Firstname`, `Surname`, `Course_Code`, `Course_Description`, `Grade`) VALUES
('96041', 'Faheem', 'Takbot', 'CS101', 'Computer Science 1', 'A'),
('97041', 'Elleanor', 'Lozano', 'CS101', 'Computer Science 1', 'C'),
('98041', 'Ameer', 'Rees', 'CS201', 'Computer Science 2', 'D'),
('99041', 'Paula', 'Pike', 'CS201', 'Computer Science 2', 'E'),
('20041', 'Ritchie', 'Terrel', 'BS102', 'Business Science 1', 'A'),
('96041', 'Faheem', 'Takbot', 'IS101', 'Information Systems 1', 'A'),
('97041', 'Elleanor', 'Lozano', 'IS101', 'Information Systems 1', 'A'),
('98041', 'Ameer', 'Rees', 'BS102', 'Business Science 1', 'B'),
('99041', 'Paula', 'Pike', 'BS102', 'Business Science 1', 'B'),
('20041', 'Ritchie', 'Terrel', 'CS101', 'Computer Science 1', 'B'),
('96041', 'Faheem', 'Takbot', 'CS101', 'Computer Science 1', 'A'),
('97041', 'Elleanor', 'Lozano', 'CS101', 'Computer Science 1', 'C'),
('98041', 'Ameer', 'Rees', 'CS201', 'Computer Science 2', 'D'),
('99041', 'Paula', 'Pike', 'CS201', 'Computer Science 2', 'E'),
('20041', 'Ritchie', 'Terrel', 'BS102', 'Business Science 1', 'A'),
('96041', 'Faheem', 'Takbot', 'IS101', 'Information Systems 1', 'A'),
('97041', 'Elleanor', 'Lozano', 'IS101', 'Information Systems 1', 'A'),
('98041', 'Ameer', 'Rees', 'BS102', 'Business Science 1', 'B'),
('99041', 'Paula', 'Pike', 'BS102', 'Business Science 1', 'B'),
('20041', 'Ritchie', 'Terrel', 'CS101', 'Computer Science 1', 'B'),
('96041', 'Faheem', 'Takbot', 'CS101', 'Computer Science 1', 'A'),
('97041', 'Elleanor', 'Lozano', 'CS101', 'Computer Science 1', 'C'),
('98041', 'Ameer', 'Rees', 'CS201', 'Computer Science 2', 'D'),
('99041', 'Paula', 'Pike', 'CS201', 'Computer Science 2', 'E'),
('20041', 'Ritchie', 'Terrel', 'BS102', 'Business Science 1', 'A'),
('96041', 'Faheem', 'Takbot', 'IS101', 'Information Systems 1', 'A'),
('97041', 'Elleanor', 'Lozano', 'IS101', 'Information Systems 1', 'A'),
('98041', 'Ameer', 'Rees', 'BS102', 'Business Science 1', 'B'),
('99041', 'Paula', 'Pike', 'BS102', 'Business Science 1', 'B'),
('20041', 'Ritchie', 'Terrel', 'CS101', 'Computer Science 1', 'B'),
('96041', 'Faheem', 'Takbot', 'CS101', 'Computer Science 1', 'A'),
('97041', 'Elleanor', 'Lozano', 'CS101', 'Computer Science 1', 'C'),
('98041', 'Ameer', 'Rees', 'CS201', 'Computer Science 2', 'D'),
('99041', 'Paula', 'Pike', 'CS201', 'Computer Science 2', 'E'),
('20041', 'Ritchie', 'Terrel', 'BS102', 'Business Science 1', 'A'),
('96041', 'Faheem', 'Takbot', 'IS101', 'Information Systems 1', 'A'),
('97041', 'Elleanor', 'Lozano', 'IS101', 'Information Systems 1', 'A'),
('98041', 'Ameer', 'Rees', 'BS102', 'Business Science 1', 'B'),
('99041', 'Paula', 'Pike', 'BS102', 'Business Science 1', 'B'),
('20041', 'Ritchie', 'Terrel', 'CS101', 'Computer Science 1', 'B'),
('96041', 'Faheem', 'Takbot', 'CS101', 'Computer Science 1', 'A'),
('97041', 'Elleanor', 'Lozano', 'CS101', 'Computer Science 1', 'C'),
('98041', 'Ameer', 'Rees', 'CS201', 'Computer Science 2', 'D'),
('99041', 'Paula', 'Pike', 'CS201', 'Computer Science 2', 'E'),
('20041', 'Ritchie', 'Terrel', 'BS102', 'Business Science 1', 'A'),
('96041', 'Faheem', 'Takbot', 'IS101', 'Information Systems 1', 'A'),
('97041', 'Elleanor', 'Lozano', 'IS101', 'Information Systems 1', 'A'),
('98041', 'Ameer', 'Rees', 'BS102', 'Business Science 1', 'B'),
('99041', 'Paula', 'Pike', 'BS102', 'Business Science 1', 'B'),
('20041', 'Ritchie', 'Terrel', 'CS101', 'Computer Science 1', 'B'),
('96041', 'Faheem', 'Takbot', 'CS101', 'Computer Science 1', 'A'),
('97041', 'Elleanor', 'Lozano', 'CS101', 'Computer Science 1', 'C'),
('98041', 'Ameer', 'Rees', 'CS201', 'Computer Science 2', 'D'),
('99041', 'Paula', 'Pike', 'CS201', 'Computer Science 2', 'E'),
('20041', 'Ritchie', 'Terrel', 'BS102', 'Business Science 1', 'A'),
('96041', 'Faheem', 'Takbot', 'IS101', 'Information Systems 1', 'A'),
('97041', 'Elleanor', 'Lozano', 'IS101', 'Information Systems 1', 'A'),
('98041', 'Ameer', 'Rees', 'BS102', 'Business Science 1', 'B'),
('99041', 'Paula', 'Pike', 'BS102', 'Business Science 1', 'B'),
('20041', 'Ritchie', 'Terrel', 'CS101', 'Computer Science 1', 'B');

-- --------------------------------------------------------

--
-- Stand-in structure for view `viewstudents`
-- (See below for the actual view)
--
CREATE TABLE `viewstudents` (
`Student_Number` varchar(20)
,`Firstname` varchar(50)
,`Surname` varchar(50)
,`Course_Code` varchar(20)
,`Course_Description` varchar(100)
,`Grade` varchar(20)
);

-- --------------------------------------------------------

--
-- Structure for view `viewstudents`
--
DROP TABLE IF EXISTS `viewstudents`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `viewstudents`  AS SELECT `students`.`Student_Number` AS `Student_Number`, `students`.`Firstname` AS `Firstname`, `students`.`Surname` AS `Surname`, `students`.`Course_Code` AS `Course_Code`, `students`.`Course_Description` AS `Course_Description`, `students`.`Grade` AS `Grade` FROM `students` ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
