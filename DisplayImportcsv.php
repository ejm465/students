  <?php
//connection string to the database
 $servername = "localhost";
 $username = "root";
 $password = "";
 $database = "Information";
 
 $conn = new mysqli($servername, $username, $password, $database);
 
  if(isset($_POST["import"])){
  $filename = $_FILES["file"]["tmp_name"];
  
  if($_FILES["file"]["size"]>0){
  $file = fopen($filename, "r");
  fgetcsv($file);   // discard the first row
  
  //inserting data from csv file to database 
  
  while(($column = fgetcsv($file))){
  $sqlInsert = "Insert into Students(Student_Number,Firstname,Surname,Course_Code,Course_Description,Grade)
  values('".$column[0]."','".$column[1]."','".$column[2]."','".$column[3]."','".$column[4]."','".$column[5]."')";
  
  $result = mysqli_query($conn, $sqlInsert);
  }
  if(!empty($result)){
  echo "CSV File has been imported to the database";
  header("Refresh:5");   // page refresh after 5 seconds
  
  }
  else{
  echo "Problem in Importing csv";
  }
  
  }
  }
  
  //web layout
  ?>
  
  
  <form class = "form-horizontal" action = "" method = "post" name = "uploadCsv" enctype = "multipart/form-data">
  <div>
  <label>Choose CSV File</label>
  <input type = "file" name = "file" accept = ".csv">
  <button type ="submit" name = "import">Import</button>
  </div>
  </form>
  
  <?php
  
  
  
  $sqlSelect = "SELECT * from viewStudents";      //selecting imported csv data from view called viewStudents
  
  $result = mysqli_query($conn, $sqlSelect);
  
  
  if(mysqli_num_rows($result)>0){
  ?>
   <table>
  <thead>
  <tr>
  <th>Student Number </th>
  <th>Firstname</th>
  <th>Surname</th>
  <th>Course Code</th>
  <th>Course Description</th>
  <th>Grade</th>
  </tr>
  </thead>
  <?php
  
  // output information from database to web
 
  while ($row = mysqli_fetch_array($result)){
  ?>
  
  <tbody>
  <tr>
  <td> <?php echo $row['Student_Number'];?></td>
  <td> <?php echo $row['Firstname'];?></td>
  <td> <?php echo $row['Surname'];?></td>
  <td> <?php echo $row['Course_Code'];?></td>
  <td> <?php echo $row['Course_Description'];?></td>
  <td> <?php echo $row['Grade'];?></td>
  </tr>
  </tbody>
  <?php } ?>
  </table>
  <?php } ?>